1. The other three patterns of computer programming are sequential, conditional and store and reuse.

2. Dr.Chuck introduces a program using 'while' that acts like an if statement. The program takes a number prints it if it is greater than zero, subtracts one and runs again wt that number until it gets to where the number is not greater than zero where it prints 'Blastoff!'.

3. Another word for looping is repeated steps.

4. The broken example of a loops is called an infinite loop because n does not change so it runs forever. 

5. The new statement we can use to exit a loop is the break statement.

6. The other loop control statement introduced in the video is the cntinue statement. The continue statement brings the loop back up to the top of the loop and continues to run. 

7. Dr.Chuck ends the video saying the while is an indefinite loop because it goes until it hits a break or until that value is not true. In the next video he will talk about definite loops. 

8. The loop we learn about in the second video is called a definite loop and it loops through a set of variables.

9. By loop idioms Dr.Chuck means patterns of how we construct loops. Some examples he introduces us to are largest so far, counting, summing, finding the average (count and sum), filtering, search using a boolean variable and smallest so far.
