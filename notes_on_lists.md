1. Dr.Chuck defines Algorithms as a set of rules or steps used to solve a problem. He defines Data Structures as a particular way of organizing data in a computer. Dr.Chuck says that we have been focusing on algorithms until now.

2. A collection is like a variable that you can put many values into. 

3. The point Dr.Chuck makes is that Python does not understand the difference between singular and plurals. 

4. We access individual items in a lsit by using their positions starting with 0 as the base position.

5. Mutable means changeable. Strings are immutable but lists are mutable.

6. The first operation presented is called concatonating using a '+', which adds two lists together to create a new list. The second operation is list slicing using ':', which takes the list from the first number "up to but not including" the second number.

7. The method count looks for certain valus in the list, extend adds things to the end of the list, index looks things up in the list, insert allows the list to be expanded in the middle, pop pulls things off the top, remove removes an item in the middle, reverse flips the order of them and sort puts them in order based on their values. You can us the append method to add elements to an empty listand you can add more things using append and it will add them to the end of the list. You can also use the in and not in operators to see if something is in or not in the list.

8. Len gives how many numbers there are in the list, max finds the greatest number in the list, min finds the smallest, sum adds all of them up, and you can find the average by taking the sum and dividing it by len.

9. The first thing Dr.Chuck showed us in this video was how to use split to turn a string into a list by dividing the sentence where there are spaces. He also mentions that you can tell split what to split on. Next he mention strip which takes off the end of the new lines and startswith('') to see if it starts with that. You can also split something twice using double delimiters by pulling out the variable and putting it into another list.

10. A guardian pattern is a way to make sure that the program does not blow up. One example is that Dr.Chucks program was blowing up when it came to a blank. In order to correct this he wrote if line == '': 
                                    print('Skip Blank')
                                    continue
