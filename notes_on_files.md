1. for accessing files Python uses the function open(). Open requires the name of the file and to the arguement to read or write it. Open returns a file handle

2. A handle is a thing that allows you to get to the file and is not the file it
self nor the data. Data is stored within the file.

3. "/n" is the newline character and it indicates to go to a new line.

4. Dr.Chuck says the most common way to treat a file when reading it is to treat it as a sequence of lines.

5. No because that is not the function of the slice command.

6. The first pattern presented does not do anything to get rid of the extra line. The second pattern presented uses rstrip to get rid of any whitespace from the right end. The third pattern presented uses skipping with continue to remove the line by pulling out the good lines. To do skipping with continue you add an if not statement after the rstrip.

7. The new construct introduced at the end of the video to deal with the situation when the program attempts to open a file that isnt there trys a file file first and if it deosn't except it then it quits. The new keywords that pair to make this construct are try and except.
  
