1. You pair program so that it is easier to check your code and come up with the best solutions. The video claims that pair programming gives you the benefits of learning that it is ok to make mistakes and helps you take advice so that you improve.

2. The two roles in pair programming are the driver and the navigator. Only having one computer allows for conversation between the two programmers and forces each programmer into their roles of driver or navigator.
