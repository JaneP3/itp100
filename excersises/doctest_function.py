
def say_hi_to(name, lang):
    if lang=='es':
        say='Hola'
    elif lang=='en':
        say='Hello'
    elif lang=='fr':
        say= 'Bonojour'
    else:
        say='I dont know this language'

    print say, ', ',name

if __name__ == "__main__":
    import doctest
    say_hi_to('Andrea', 'es')
    say_hi_to('Colton', 'fr')
    say_hi_to('Jane', 'en')
    say_hi_to('Meiji', 'es')
    doctest.testmod()

