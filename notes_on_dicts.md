1. A collection is a variable that you can put multiple pieces of information in.

2. My comparison between lists and dictionaries is that lists are like a new box of colored pencils and dictionaries are like old pencil cases that have all you school supplies mixed in there.

3. Dr.Chuck tells us the synonym for dictionaries is associative arrays.

4. Some examples of Dictionary literals being assigned to variables are: 
jjj = ( 'Sam' : 2, 'Lily' : 5, 'Chloe' : 60), ppp = ( 'Turkey' : 15, 'Stuffing' : 25, 'Potatoes' : 35)

5. in the second video Dr.Chuck introduces an application making a histogram that gives the most repeated thing by adding one everytime you see that thing.

6. ccc = dict(1)
   ccc['csev'] = 1
   ccc['cwen'] = 1
   print(ccc)
('csev': 1, 'cwen': 1)
   ccc['cwen'] = ccc['cwen'] + 1
   print (ccc)
('csev': 1, 'cwen': 2)

7. The dictionary operation that makes this common operation shorter is get. You use get by providing a default value of zero and adding one. 

8. In the third video Dr.Chuch leads the application that goes through all the keys in a loop and pulling out the values.

