1. Dr.Chuck states that tuples are like lists because they have eclements which are indexed starting at zero. I agree because they act exactly like a list and the only difference is that you cant modify a tuple.

2. Tuples are essentially different from lists in that you can't modify a tuple. I think there is a need for tuples because they are more efficient than lists.

3. A tupl assignment is when you assign variables to a tuple by putting it on the left hand side of an assignment statement. While assigning tuples you use round brackets instead of square ones and sometime you don't even need brackets. An example of a tuple assignment is: (x, y) = (4, 'fred') you can do print(y) and get fred.

4. In the second video I learned how to make a tuple into a temporary list by doing: c = {'a':10, 'b':1, 'c':22}
     tmp = list()
     for k, v in c.items() :
         tmp.append( (v, k) )

5. I did not really understand list comprehensions but I think I could if it was explained again.
