1. The name of this architecture is Transport Control Protocol (TCP).

2. The name of the layer Dr.Chuck says we are going to be looking at is the Transport layer. The two layers below that are called the Internet and Link layers.

3. We will be assuming that there is a nice reliable pipe that goes from point A to point B. There is a process running at each end of the connection.

4. An internet socket is the "phone call" between two processes.

5. A TCP Port is the number that is for the application.

6. Sockets exist at the Application layer

7. The World Wide Web uses the network protocol http and operates at the Application layer. 
